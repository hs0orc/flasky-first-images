from flask import render_template, redirect, url_for, abort, flash, request,\
    current_app, make_response
from flask_login import login_required, current_user
from flask_sqlalchemy import get_debug_queries

from .forms import EditProfileForm, EditProfileAdminForm, PostForm,\
    CommentForm
from .. import db
from ..models import Permission, Role, User, Post, Comment, Pictures  # UploadPicture, Like
from ..decorators import admin_required, permission_required
from . import main
from flask import g
# from . import boto3
# import boto3


@main.route('/testing-icons', methods=["POST", "GET"])
def testing_user_name():
   #  print('current user name =', current_user.username)
    return render_template('testing_icons.html')

# @main.route('/testing-get-user-name', methods=["POST", "GET"])
# def testing_user_name():
#     g.user = current_user.get_id()  # returns 3 for ch1na account
#     print('g user =', g.user)
#     user = User.query.filter_by(id=current_user.get_id()).first()  # returns ch1na
#     print('user = ', user)
#     return 'ok'

@main.route('/testing-pictures', methods=["POST", "GET"])
def testing_pictures():
    picture = Pictures.query.all()
    # for x in picture:
    #     print(x.link)
    return render_template('testing_pictures.html', picture=picture)


@main.route('/add-pictures', methods=["POST", "GET"])
def add_pictures_to_database():
    ''' This function takes whats in the bucket bototest69
    and inserts it into the PostgreSQL whether its local postgres@localhost or heroku/aws
    '''
    import boto3
    error = False
    s3_client = boto3.client('s3',
                             aws_access_key_id='AKIASFUPF56QHV7FFSNC',
                             aws_secret_access_key='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',
                             region_name='us-west-1'
                             )

    paginator = s3_client.get_paginator('list_objects')
    lst = []
    [lst.append(page) for page in paginator.paginate(Bucket='bototest69').search("Contents[?Size > `0`][]")]
    # print('lst 1 = ', lst)
    another_lst = []
    for key in lst:
        # print(x['Key'])
        another_lst.append(key['Key'])
    # print('another lst = ', another_lst)

    final_lst = []
    for pet_link in another_lst:
        final_lst.append('https://bototest69.s3.us-west-1.amazonaws.com/' + pet_link)
    # print('final links =', final_lst)
   # test = 'https://bototest69.s3.us-west-1.amazonaws.com/china.jpg'
    for pic in final_lst:
        picture = Pictures(link=pic)
        try:
            db.session.add(picture)
            db.session.commit()
        except:
            error = True
            db.session.rollback()
        # print(sys.exc_info())
        finally:
            db.session.close()
    if error:
        return 'error'
    return render_template('pictures.html')

@main.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['FLASKY_SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(
                'Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n'
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response


@main.route('/shutdown')
def server_shutdown():
    if not current_app.testing:
        abort(404)
    shutdown = request.environ.get('werkzeug.server.shutdown')
    if not shutdown:
        abort(500)
    shutdown()
    return 'Shutting down...'

@main.route('/roles', methods=['GET', 'POST'])
def roles_make():
    ''' import Roles? '''
    role = Role.insert_roles()
    # role = Role()
    # role.insert_roles()
    # print('role query all = ', role.query.all())
    return 'ok'
@main.route('/', methods=['GET', 'POST'])
def index():
    ''' Line 96/97 use "g current_user.get_id()" to get current user id. Perhaps another way would be to initiate a User object, ie.
    user = User.query.all()
    then do user.id
    Get post id this way if needed.
    To get username use g.user = current_user.username
    '''
    print('current user admin = ', current_user.is_administrator())
    form = PostForm()
    # if current_user.can(Permission.WRITE_ARTICLES) and \
    if form.validate_on_submit():
        g.user = current_user.get_id()
        post = Post(body=form.body.data,
                    author_id=g.user)
        print('body =', form.body.data)
        # print('body html =', form.body_html.data)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    show_followed = False
    if current_user.is_authenticated:
        show_followed = bool(request.cookies.get('show_followed', ''))
    if show_followed:
        query = current_user.followed_posts
    else:
        query = Post.query
    pagination = query.order_by(Post.timestamp.desc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    posts = pagination.items
    return render_template('index.html', form=form, posts=posts,
                           show_followed=show_followed, pagination=pagination)


    # form = PostForm()
    # # if current_user.can(Permission.WRITE_ARTICLES) and \
    # #         form.validate_on_submit():
    # if form.validate_on_submit():
    #     print('body =', form.body.data)
    #     print('body html =', form.body_html.data)
    #     print('author =', current_user._get_current_object())
    #     # post = Post(body_html=form.body.data, body=form.body.data,
    #     #             author_id=current_user._get_current_object())
    #
    #     try:
    #         post = Post(body_html=form.body.data, body=form.body.data)
    #         db.session.add(post)
    #         db.session.commit()
    #
    #     except:
    #         db.session.rollback()
    #     finally:
    #         db.session.close()
    #     return redirect(url_for('.index'))
    # #return 'ok'
    # return render_template('index.html')
    # page = request.args.get('page', 1, type=int)
    # show_followed = False
    # if current_user.is_authenticated:
    #     show_followed = bool(request.cookies.get('show_followed', ''))
    # if show_followed:
    #     query = current_user.followed_posts
    # else:
    #     query = Post.query
    # pagination = query.order_by(Post.timestamp.desc()).paginate(
    #     page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
    #     error_out=False)
    # posts = pagination.items
    # return render_template('index.html', form=form, posts=posts,
    #                        show_followed=show_followed, pagination=pagination)
   # return render_template('index.html', form=form, posts=post)


@main.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    pagination = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    posts = pagination.items
    return render_template('user.html', user=user, posts=posts,
                           pagination=pagination)


@main.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user)
        flash('Your profile has been updated.')
        return redirect(url_for('.user', username=current_user.username))
    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', form=form)


@main.route('/edit-profile/<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(id):
    user = User.query.get_or_404(id)
    form = EditProfileAdminForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        db.session.add(user)
        flash('The profile has been updated.')
        return redirect(url_for('.user', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me
    return render_template('edit_profile.html', form=form, user=user)

# @main.route('/posts', methods=['GET', 'POST'])
# def posts():
#     posts = PostForm()
#     return render_template('_posts.html', posts=posts)


@main.route('/post/<int:id>', methods=['GET', 'POST'])
def post(id):
    post = Post.query.get_or_404(id)
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(body=form.body.data,
                          post=post,
                          author=current_user._get_current_object())
        db.session.add(comment)
        flash('Your comment has been published.')
        return redirect(url_for('.post', id=post.id, page=-1))
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (post.comments.count() - 1) // \
            current_app.config['FLASKY_COMMENTS_PER_PAGE'] + 1
    pagination = post.comments.order_by(Comment.timestamp.asc()).paginate(
        page, per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    comments = pagination.items
    return render_template('post.html', posts=[post], form=form,
                           comments=comments, pagination=pagination)


@main.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    post = Post.query.get_or_404(id)
    if current_user != post.author and \
            not current_user.can(Permission.ADMINISTER):
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.body = form.body.data
        db.session.add(post)
        flash('The post has been updated.')
        return redirect(url_for('.post', id=post.id))
    form.body.data = post.body
    return render_template('edit_post.html', form=form)


@main.route('/follow/<username>')
@login_required
@permission_required(Permission.FOLLOW)
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    if current_user.is_following(user):
        flash('You are already following this user.')
        return redirect(url_for('.user', username=username))
    current_user.follow(user)
    flash('You are now following %s.' % username)
    return redirect(url_for('.user', username=username))


@main.route('/unfollow/<username>')
@login_required
@permission_required(Permission.FOLLOW)
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    if not current_user.is_following(user):
        flash('You are not following this user.')
        return redirect(url_for('.user', username=username))
    current_user.unfollow(user)
    flash('You are not following %s anymore.' % username)
    return redirect(url_for('.user', username=username))


@main.route('/followers/<username>')
def followers(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    pagination = user.followers.paginate(
        page, per_page=current_app.config['FLASKY_FOLLOWERS_PER_PAGE'],
        error_out=False)
    follows = [{'user': item.follower, 'timestamp': item.timestamp}
               for item in pagination.items]
    return render_template('followers.html', user=user, title="Followers of",
                           endpoint='.followers', pagination=pagination,
                           follows=follows)


@main.route('/followed-by/<username>')
def followed_by(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    pagination = user.followed.paginate(
        page, per_page=current_app.config['FLASKY_FOLLOWERS_PER_PAGE'],
        error_out=False)
    follows = [{'user': item.followed, 'timestamp': item.timestamp}
               for item in pagination.items]
    return render_template('followers.html', user=user, title="Followed by",
                           endpoint='.followed_by', pagination=pagination,
                           follows=follows)


@main.route('/all')
@login_required
def show_all():
    resp = make_response(redirect(url_for('.index')))
    resp.set_cookie('show_followed', '', max_age=30*24*60*60)
    return resp


@main.route('/followed')
@login_required
def show_followed():
    resp = make_response(redirect(url_for('.index')))
    resp.set_cookie('show_followed', '1', max_age=30*24*60*60)
    return resp


@main.route('/moderate')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate():
    page = request.args.get('page', 1, type=int)
    pagination = Comment.query.order_by(Comment.timestamp.desc()).paginate(
        page, per_page=current_app.config['FLASKY_COMMENTS_PER_PAGE'],
        error_out=False)
    comments = pagination.items
    return render_template('moderate.html', comments=comments,
                           pagination=pagination, page=page)


@main.route('/moderate/enable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_enable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled = False
    db.session.add(comment)
    return redirect(url_for('.moderate',
                            page=request.args.get('page', 1, type=int)))


@main.route('/moderate/disable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_disable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled = True
    db.session.add(comment)
    return redirect(url_for('.moderate',
                            page=request.args.get('page', 1, type=int)))
